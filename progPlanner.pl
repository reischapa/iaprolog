#!/usr/bin/prolog

%Elimina todas as ocorrencias de um termo ou atomo de uma lista.
delAllElem(_,[],[]):-!.
delAllElem(H,[H|T1],L):-delAllElem(H,T1,L),!.
delAllElem(H,[A|T1],[A|L]):-delAllElem(H,T1,L),!.

%O mesmo, para todos os elementos de uma lista.
delAllListElem([],O,O).
delAllListElem([Head|Tail],I,O):- 
    delAllElem(Head,I,E), delAllListElem(Tail,E,O).


%Verifica se a primeira lista � completamente contida na segunda.
%Utilizado para verificar se um estado � objetivo.
isSubList([],_):-!.
isSubList([H|T],L):- member(H,L),isSubList(T,L).

%isSubList(_,[]):-!.
%isSubList(L,[H|T]):- member(H,L),isSubList(T,L). N�o foi necess�rio, s� se se tivesse conseguido planeamento regressivo.

extractDataAct(Name,Cond,Result,Restrict):-
    accao(nome:Name,condicoes:Cond,efeitos:Result,restricoes:Restrict).


%Extrai os fluentes presentes na primeira lista. Fluentes positivos para a segunda e negativos para a terceira.

extractEffects([],[],[]).
extractEffects([H|T],P,[H|N]):-isDashed(H),extractEffects(T,P,N),!.
extractEffects([H|T],[H|P],N):-extractEffects(T,P,N).

%Verifica as restri��es.
verifyConditions([]).
verifyConditions([H|T]):- H , verifyConditions(T).

%Verifica se tem um functor tra�o, ou seja, se � um efeito negativo.
isDashed(E):- functor(E,(-),_).
%Remove o tra�o, a um elemento, ou a todos os elementos de uma lista.
removeDash(I,O):- -O=I.
removeDashAll([],[]).
removeDashAll([H|T1],[E|T2]):- removeDash(H,E), removeDashAll(T1,T2).

deleteReps([],[]).
deleteReps([H|T],L):-member(H,T),deleteReps(T,L),!.
deleteReps([H|T],[H|L]):-deleteReps(T,L).


%Predicado que aplica a ac��o de maneira progressiva
applyAction(ActName,State,Result):- 
    extractDataAct(ActName,Cond,Delta,Res),
    
    isSubList(Cond,State),
    verifyConditions(Res),
    extractEffects(Delta,P,N),
    
    removeDashAll(N,O),
    delAllListElem(O,State,I), 
    append(P,I,Temp2),
    deleteReps(Temp2,Result).
    

%Cria todos os caminhos com profundidade N que permitem ir de um estado pedido a outro.
createNPath(0,State,Final,[State]):- 
    isSubList(Final,State).

createNPath(N,Init,Final,[Init,ActName|Tail]):- 
    N > 0,
    E is N-1, 
    applyAction(ActName,Init,Res),
    createNPath(E,Res,Final,Tail).

%Iterative deepening search, planeamento Progressivo, utiliza createNPath, para procurar as solu��es de custo m�nimo, e devolv�-las. 
% aumentando o passo at� encontrar as solu��es m�nimas

idsearch(I,G,Result):-
   assert(proceed(eraFixeTerIfThenElses)),
   ids(0,I,G,Result). %come�a em 0

%Fun��es adjuvantes para a fun��o anterior.
%O conte�do do assert � irrelevante, desde que exista.
ids(N,I,G,Result):-
    createNPath(N,I,G,Result),
    retractall(proceed(eraFixeTerIfThenElses)).
    
    
ids(N,I,G,Result):-
   proceed(eraFixeTerIfThenElses),
   E is N+1,
   ids(E,I,G,Result).

   
%Devolve o plano, conforme enunciado.

plano(L):- %autom�tico
   inicial(I),
   objectivos(G),
   idsearch(I,G,L).

plano(I,G,L):- %definido pelo utilizador
   idsearch(I,G,L).

plano:- %mostra s� output
   plano(L),
   initialOutput(L).

%Extrai da estrutura devolvida por plano/1 ou plano/3 a sequencia de planos a seguir. 
getActions([_],[]).
getActions([_,Act|Tail],[Act|T2]):-
   getActions(Tail,T2). 



%Predicado recursivo de output. So good... #wishIHadOutputStreams
%Nomes diferentes para s� existir um entry point, de modo a diferenciar o primeiro conjunto de frases dos outros.
initialOutput([Init,Action|Tail]):-
    write('Initial Situation: '),
    write(Init),
    nl,
    write('Action performed: '),
    write(Action),
    nl,
    actionOutput(Tail).

actionOutput([Init,Action|Tail]):-
    write('Situation: '),
    write(Init),
    nl,
    write('Action performed: '),
    write(Action),
    nl,
    actionOutput(Tail).


actionOutput([Goal]):-
    write('Goal '),
    write(Goal),
    write(' satisfied'),
    nl.
