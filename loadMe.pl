initialize(Case):- ['progPlanner.pl'], loadCase(Case).

clearAllPlanCases:-
   retractall(accao(_,_,_,_)),
   retractall(inicial(X)),
   retractall(objectivos(X)),
   retractall(currentProblem(X)).

loadCase(robot):-!,
   clearAllPlanCases,
   assert(currentProblem(robot)),
   assert(
        accao(nome : moveRobotFromTo(X,Y),
         condicoes : [robotAt(X),adj(X,Y)],
           efeitos : [robotAt(Y),-robotAt(X)],
        restricoes : [(X\==Y)])
    ),

   assert(
        accao(nome : moveRobotAndObjectFromTo(O,X,Y),
         condicoes : [robotAt(X),adj(X,Y),objAt(O,X)],
           efeitos : [robotAt(Y),objAt(O,Y),-objAt(O,X),-robotAt(X)],
        restricoes : [(X\==Y),(O\==Y),(O\==X)])
   ),

   assert(
    inicial([robotAt(s1),objAt(b1,s1),objAt(b2,s2),objAt(b3,s3),adj(s1,s2),adj(s2,s3),adj(s2,s1),adj(s3,s2)]) 
   ),

   assert(
    objectivos([robotAt(s3),objAt(b1,s3),objAt(b2,s3),objAt(b3,s3),adj(s1,s2),adj(s2,s3),adj(s2,s1),adj(s3,s2)])
   ).

loadCase(sapatos):-!,
   clearAllPlanCases,
   assert(currentProblem(sapatos)),
   assert(
        accao(nome : calcarSap(Pe),
         condicoes : [meia(Pe)],
           efeitos : [sapato(Pe)],
        restricoes : [])
    ),

   assert(
        accao(nome : calcarMeia(Pe),
         condicoes : [pe(Pe)],
           efeitos : [meia(Pe)],
        restricoes : [])  
   ),

   assert(
    inicial([pe(dir),pe(esq)])
   ),

   assert(
    objectivos([sapato(esq),sapato(dir)])
   ).

loadCase(sussman):-!,
   clearAllPlanCases,
   assert(currentProblem(sussman)),
   assert(
        accao(nome : putTable(X),
         condicoes : [on(X,Z),clear(X)], 
           efeitos : [clear(Z),on(X,mesa),-on(X,Z)], 
        restricoes : [(X\==Z),(Z\==mesa)])
    ),

   assert(
        accao(nome : putOn(X,Y),
         condicoes : [on(X,Z),clear(X),clear(Y)], 
           efeitos : [clear(Z),on(X,Y),-on(X,Z),-clear(Y)], 
        restricoes : [(Y\==mesa),(X\==Y),(X\==Z),(Y\==Z)])  
   ),

   assert(
        inicial([clear(c),on(c,a),on(a,mesa),clear(b),on(b,mesa)])
   ),

   assert(
        objectivos([on(a,b),on(b,c)])
   ).

loadCase(compras):-!,
   clearAllPlanCases,
   assert(currentProblem(compras)),
   assert(
        accao(nome : go(X,Y),
         condicoes : [at(X),place(Y)], 
           efeitos : [at(Y),-at(X)], 
        restricoes : [(X\==Y)])
    ),

   assert(
        accao(nome : buy(X),
         condicoes : [sell(L,X),at(L)],
           efeitos : [have(X)], 
        restricoes : [])
   ),

   assert(
        inicial([at(home),place(home),place(hws),place(super),sell(super,banana),sell(hws,drill),sell(super,milk)])
        
   ),

   assert(
        objectivos([have(milk),have(drill),have(banana),at(home)])
   ).


loadCase(blocos):-!,
   clearAllPlanCases,
   assert(currentProblem(blocos)),
   assert(
        accao(nome : putTable(X),
         condicoes : [on(X,Z),clear(X)], 
           efeitos : [clear(Z),on(X,mesa),-on(X,Z)], 
        restricoes : [(X\==Z),(Z\==mesa)])
    ),

   assert(
        accao(nome : putOn(X,Y),
         condicoes : [on(X,Z),clear(X),clear(Y)], 
           efeitos : [clear(Z),on(X,Y),-on(X,Z),-clear(Y)], 
        restricoes : [(Y\==mesa),(X\==Y),(X\==Z),(Y\==Z)])  
   ),

   assert(
        inicial([clear(b),on(b,a),on(a,mesa),clear(d),on(d,c),on(c,mesa)])
   ),

   assert(
        objectivos([on(a,c),on(b,d)])
   ).

loadCase(viagem):-!,
   clearAllPlanCases,
   assert(currentProblem(viagem)),

   assert(
       accao(nome : go(X,Y),
        condicoes : [at(X),road(X,Y)], 
          efeitos : [at(Y),-at(X)], 
       restricoes : [(X\==Y)])
    ),

   assert(
        inicial([at(lisboa),road(lisboa,evora),road(evora,faro),road(lisboa,coimbra),road(coimbra,porto),road(evora,coimbra)])
   ),

   assert(
        objectivos([at(porto)])
   ).

